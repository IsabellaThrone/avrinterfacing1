/*
 * GccApplication5.c
 *
 * Created: 9/27/2019 7:49:33 PM
 * Author : reham
 */ 
#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#define SET_BIT(reg,bit)   reg|=(1<<bit)
#define CLR_BIT(reg,bit)   reg&=~(1<<bit)
#define Motor_Port PORTC
#define Motor_E_Port PORTD
#define ENA 4


void MotorCW()
{
	SET_BIT(Motor_E_Port,ENA);
	SET_BIT(Motor_Port,3);
	CLR_BIT(Motor_Port,4);
}


int main(void)
{
   DDRB&=~(1<<0);
   DDRB&=~(1<<4);
   DDRA|=(1<<3);
   DDRD|=(1<<3);
   DDRD|=(1<<4);
   DDRC|=(1<<3);
   DDRC|=(1<<4);
    while (1) 
    {
		if(PINB&0x01)
		{
		    SET_BIT(PORTD,3);
		    _delay_us(1000);
		}
		else
		{
			CLR_BIT(PORTD,3);
			_delay_us(100);
		}
		
		if(PINB&(1<<4))
		{
			SET_BIT(PORTA,3);
			_delay_us(1000);
		}
		else
		{
			CLR_BIT(PORTA,3);
			_delay_us(100);
		}
		
		MotorCW();
		
    }
}

